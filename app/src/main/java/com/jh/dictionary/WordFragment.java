package com.jh.dictionary;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.jh.dictionary.data.DictionaryProvider;
import com.jh.dictionary.data.Word;

public class WordFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {
    private static final String ARG_ID = "id";

    private TextView wordName;
    private TextView language;
    private TextView translation;

    public WordFragment() {
    }

    public static WordFragment newInstance(long id) {
        WordFragment f = new WordFragment();
        Bundle args = new Bundle();
        args.putLong(ARG_ID, id);
        f.setArguments(args);
        return f;
    }

    private long getWordId() {
        if (getArguments() != null) {
            return getArguments().getLong(ARG_ID, -1);
        }
        return -1;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_word, container, false);
        setRetainInstance(true);
        setHasOptionsMenu(true);

        wordName = (TextView) rootView.findViewById(R.id.word);
        language = (TextView) rootView.findViewById(R.id.language);
        translation = (TextView) rootView.findViewById(R.id.translation);

        getLoaderManager().initLoader(0, null, this);
        return rootView;
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_word, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.delete:
                if (getActivity() != null) {
                    getActivity().getContentResolver().delete(
                            Uri.withAppendedPath(DictionaryProvider.WORDS_CONTENT_URI,
                            String.valueOf(getWordId())), null, null);
                    Toast.makeText(getActivity(), R.string.word_delete, Toast.LENGTH_SHORT).show();
                    getActivity().getSupportFragmentManager().popBackStack();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int loaderID, Bundle bundle) {
        return new CursorLoader(getActivity(),
                Uri.withAppendedPath(DictionaryProvider.WORDS_CONTENT_URI, String.valueOf(getWordId())),
                null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        Word word = DictionaryProvider.getWord(cursor);
        if (word != null) {
            wordName.setText(word.getWord());
            translation.setText(word.getTranslation());
            language.setText(word.getLanguage());
        } else {
            if (getActivity() != null) {
                Toast.makeText(getActivity(), R.string.word_load_fail, Toast.LENGTH_SHORT).show();
                //getActivity().getSupportFragmentManager().popBackStack(); show empty card is better
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}
