package com.jh.dictionary;

import android.app.SearchManager;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.support.v4.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.jh.dictionary.data.DictionaryProvider;

public class DictionaryFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {
    private static final int URL_LOADER = 0;
    private static final String ARG_SEARCH = "search";

    private GridView list;
    private SimpleCursorAdapter wordsCursor;
    private String searchQuery;
    private SearchView searchView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        setRetainInstance(true);
        setHasOptionsMenu(true);

        list = (GridView) rootView.findViewById(R.id.list);
        ImageView btnAdd = (ImageView) rootView.findViewById(R.id.addButton);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null) ((MainActivity) getActivity()).showNewWord();
            }
        });

        getLoaderManager().initLoader(URL_LOADER, null, this);

        String[] from = new String[] { DictionaryProvider.WORD_NAME,
                DictionaryProvider.WORD_TRANSLATE};
        int[] to = new int[] {android.R.id.text1, android.R.id.text2};

        wordsCursor = new SimpleCursorAdapter(getActivity(), R.layout.item_word, null, from, to,
                CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
        list.setAdapter(wordsCursor);
        TextView emptyList = (TextView) rootView.findViewById(android.R.id.empty);
        list.setEmptyView(emptyList);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (getActivity() != null) {
                    ((MainActivity) getActivity()).showWord(id);
                }
            }
        });

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_main, menu);

        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        MenuItem searchItem = menu.findItem(R.id.search);
        searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        if (searchQuery != null && !searchQuery.isEmpty()) {
            searchItem.expandActionView();
            searchView.setQuery(searchQuery, false);
        }
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                Bundle args = new Bundle();
                args.putString(ARG_SEARCH, s);
                getLoaderManager().restartLoader(URL_LOADER, args, DictionaryFragment.this);
                searchQuery = s;
                return false;
            }
        });
    }

    @Override
    public Loader<Cursor> onCreateLoader(int loaderID, Bundle bundle) {
        switch (loaderID) {
            case URL_LOADER:
                String search = "";
                if (bundle != null)
                    search = DictionaryProvider.getSQLString(bundle.getString(ARG_SEARCH, ""));
                return new CursorLoader(
                        getActivity(),      // Parent activity context
                        DictionaryProvider.WORDS_CONTENT_URI,   // Table to query
                        null,               // Projection to return
                        DictionaryProvider.getWordLike(search), // No selection clause
                        null,               // No selection arguments
                        null                // Default sort order
                );
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        wordsCursor.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        wordsCursor.swapCursor(null);
    }
}
