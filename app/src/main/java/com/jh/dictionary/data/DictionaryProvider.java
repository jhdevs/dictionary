package com.jh.dictionary.data;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;

public class DictionaryProvider extends ContentProvider {
    // DB constants
    private static final String DB_NAME = "dictionary";
    private static final int DB_VERSION = 1;
    private static final String WORDS_TABLE = "words";
    private static final String WORD_ID = "_id";
    public static final String WORD_NAME = "name";
    public static final String WORD_TRANSLATE = "translate";
    private static final String WORD_LANG = "lang";

    // Create table sql
    private static final String DB_CREATE = "create table " + WORDS_TABLE + "("
            + WORD_ID + " integer primary key autoincrement, "
            + WORD_NAME + " text, " + WORD_TRANSLATE + " text," + WORD_LANG + " text" + ");";

    // URI constants
    private static final String AUTHORITY = "com.jh.dictionary.Data";
    private static final String WORDS_PATH = "words";
    public static final Uri WORDS_CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/"
            + WORDS_PATH);
    private static final String WORDS_CONTENT_TYPE = "vnd.android.cursor.dir/vnd." + AUTHORITY +
            "." + WORDS_PATH;
    private static final String WORDS_CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd." + AUTHORITY +
            "." + WORDS_PATH;


    private static final int URI_WORDS = 1;
    private static final int URI_WORD_ID = 2;

    private static final UriMatcher uriMatcher;
    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTHORITY, WORDS_PATH, URI_WORDS);
        uriMatcher.addURI(AUTHORITY, WORDS_PATH + "/#", URI_WORD_ID);
    }

    private DBHelper dbHelper;
    private SQLiteDatabase db;

    @Override
    public boolean onCreate() {
        dbHelper = new DBHelper(getContext());
        return false;
    }

    private String getSelection(Uri uri, String selection) {
        switch (uriMatcher.match(uri)) {
            case URI_WORDS: // общий Uri
                break;
            case URI_WORD_ID: // Uri с ID
                String id = uri.getLastPathSegment();
                if (selection == null || selection.isEmpty()) {
                    selection = WORD_ID + " = " + id;
                } else {
                    selection += " AND " + WORD_ID + " = " + id;
                }
                break;
            default:
                throw new IllegalArgumentException("Wrong URI: " + uri);
        }
        return selection;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        db = dbHelper.getWritableDatabase();
        Cursor cursor = db.query(WORDS_TABLE, projection, getSelection(uri, selection), selectionArgs, null, null, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(), WORDS_CONTENT_URI);
        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        switch (uriMatcher.match(uri)) {
            case URI_WORDS:
                return WORDS_CONTENT_TYPE;
            case URI_WORD_ID:
                return WORDS_CONTENT_ITEM_TYPE;
        }
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        long id = db.insert(WORDS_TABLE, null, values);
        getContext().getContentResolver().notifyChange(uri, null);
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int count = db.delete(WORDS_TABLE, getSelection(uri, selection), selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        throw new IllegalArgumentException("Unsupported URI " + uri);
    }

    public static String getSQLString(String s) {
        return s.replace('\'', '`');
    }

    public static Word getWord(Cursor cursor) {
        if (cursor != null && cursor.moveToFirst()) {
            return new Word(cursor.getString(1), cursor.getString(2), cursor.getString(3));
        }
        return null;
    }

    public static ContentValues getContentValues(Word word) {
        if (word == null) return null;
        ContentValues cv = new ContentValues();
        cv.put(WORD_NAME, word.getWord());
        cv.put(WORD_TRANSLATE, word.getTranslation());
        cv.put(WORD_LANG, word.getLanguage());
        return cv;
    }

    public static String getWordIs(String s) {
        return "(lower(" + WORD_NAME + ")=lower('" + s + "'))";
    }

    public static String getWordLike(String s) {
        return "(" + WORD_NAME + " like '%" + s + "%' or " + WORD_TRANSLATE + " like '%" + s + "%')";
    }

    private class DBHelper extends SQLiteOpenHelper {

        public DBHelper(Context context) {
            super(context, DB_NAME, null, DB_VERSION);
        }

        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DB_CREATE);
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }
    }
}
