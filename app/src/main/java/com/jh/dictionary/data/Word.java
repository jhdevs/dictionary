package com.jh.dictionary.data;

public class Word {
    private final String word;
    private final String translation;
    private final String language;

    public Word (String word, String translation, String language) {
        this.word = word;
        this.translation = translation;
        this.language = language;
    }

    public String getWord() {
        return word;
    }

    public String getTranslation() {
        return translation;
    }

    public String getLanguage() {
        return language;
    }
}
