package com.jh.dictionary;

import android.content.AsyncQueryHandler;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.jh.dictionary.data.DictionaryProvider;
import com.jh.dictionary.data.Word;
import com.jh.dictionary.translation.TranslationCode;
import com.jh.dictionary.translation.YandexTranslator;

public class NewWordFragment extends Fragment implements View.OnClickListener  {
    private EditText newWord;
    private TextView translateResult;
    private TextView translateState;
    private ProgressBar progress;
    private Button btnSave;
    private Word result = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_new_word, container, false);
        setRetainInstance(true);

        newWord = (EditText) rootView.findViewById(R.id.newWord);
        translateState = (TextView) rootView.findViewById(R.id.translateState);
        translateResult = (TextView) rootView.findViewById(R.id.translateResult);
        progress = (ProgressBar) rootView.findViewById(R.id.progressBar);
        btnSave = (Button) rootView.findViewById(R.id.saveButton);
        btnSave.setOnClickListener(this);
        Button btnCancel = (Button) rootView.findViewById(R.id.cancelButton);
        btnCancel.setOnClickListener(this);

        newWord.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                synchronized (NewWordFragment.this) {
                    result = null;
                    btnSave.setEnabled(false);
                    progress.setVisibility(View.VISIBLE);
                    YandexTranslator.translate(NewWordFragment.this, s);
                }
            }
        });
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        newWord.requestFocus();
        InputMethodManager mgr = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.showSoftInput(newWord, InputMethodManager.SHOW_IMPLICIT);
    }

    @Override
    public void onPause() {
        newWord.requestFocus();
        InputMethodManager mgr = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(newWord.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        super.onPause();
    }

    public void setTranslate(Word word, TranslationCode code) {
        synchronized (this) {
            result = null;
            switch (code) {
                case TRANSLATION_OK:
                    translateState.setText(word.getLanguage());
                    translateResult.setText(word.getTranslation());
                    result = word;
                    break;
                case TRANSLATION_EMPTY:
                    translateState.setText(null);
                    translateResult.setText(null);
                    break;
                case BIG_TEXT:
                    translateState.setText(R.string.big_text);
                    translateResult.setText(null);
                    break;
                case TRANSLATION_ERROR:
                    translateState.setText(R.string.word_bad_service);
                    translateResult.setText(null);
                    break;
                case NO_TRANSLATION:
                    translateState.setText(R.string.word_no_translation);
                    translateResult.setText(null);
                    break;
                case WORD_IN_DICTIONARY:
                    translateState.setText(R.string.word_in_dictionary);
                    translateResult.setText(word.getTranslation());
                    break;
            }
            progress.setVisibility(View.INVISIBLE);
            btnSave.setEnabled(code == TranslationCode.TRANSLATION_OK);
        }
    }

    @Override
    public void onClick(View v) {
        YandexTranslator.stop();
        switch(v.getId()) {
            case R.id.saveButton:
                ContentValues cv = DictionaryProvider.getContentValues(result);
                if (cv != null && getActivity() != null) {
                    AsyncQueryHandler handler = new AsyncQueryHandler(getActivity().getContentResolver()) {};
                    handler.startInsert(0, null, DictionaryProvider.WORDS_CONTENT_URI, cv);
                    Toast.makeText(getActivity(), R.string.word_add, Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.cancelButton:
                break;
        }
        if (getActivity() != null) getActivity().getSupportFragmentManager().popBackStack();
    }

    //runs in bg thread
    public Word hasWord(String s) {
        if (getActivity() != null) {
            Cursor cursor = getActivity().getContentResolver().query(DictionaryProvider.WORDS_CONTENT_URI,
                    null, DictionaryProvider.getWordIs(s), null, null);
            return DictionaryProvider.getWord(cursor);
        }
        return null;
    }
}
