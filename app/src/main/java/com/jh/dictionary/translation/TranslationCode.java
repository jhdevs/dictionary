package com.jh.dictionary.translation;

public enum TranslationCode {
    TRANSLATION_OK,
    TRANSLATION_EMPTY,
    WORD_IN_DICTIONARY,
    BIG_TEXT,
    TRANSLATION_ERROR,
    NO_TRANSLATION
}