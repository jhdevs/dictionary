package com.jh.dictionary.translation;

import android.os.AsyncTask;

import com.jh.dictionary.NewWordFragment;
import com.jh.dictionary.data.DictionaryProvider;
import com.jh.dictionary.data.Word;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.net.URLEncoder;

import javax.net.ssl.HttpsURLConnection;

public class YandexTranslator extends AsyncTask<String, Integer, TranslationCode> {
    private static YandexTranslator translator = null;
    private static String request = "https://translate.yandex.net/api/v1.5/tr.json/translate?key=trnsl.1.1.20150224T213058Z.b13a02ec8bfd0507.6c16e789f40988b14529df8da8aa358d6ee232b4&lang=ru&format=plain&options=1&text=";
    private static String encoding = "UTF-8";

    private final WeakReference<NewWordFragment> newWordReference;
    private Word result;

    private YandexTranslator(NewWordFragment refFragment) {
        newWordReference = new WeakReference<NewWordFragment>(refFragment);
    }

    @Override
    protected TranslationCode doInBackground(String... params) {
        TranslationCode ret = TranslationCode.TRANSLATION_OK; //ok
        result = null;
        try {
            if (params[0].length() == 0) return TranslationCode.TRANSLATION_EMPTY;
            if (params[0].length() > 100) return TranslationCode.BIG_TEXT;
            String query = DictionaryProvider.getSQLString(params[0]).trim();

            NewWordFragment refFragment = newWordReference.get();
            if (refFragment != null) {
                result = refFragment.hasWord(query);
                if (result != null) return TranslationCode.WORD_IN_DICTIONARY;
            }

            URL url = new URL(request + URLEncoder.encode(query, encoding));

            final HttpsURLConnection uc = (HttpsURLConnection) url.openConnection();
            uc.setRequestMethod("GET");
            uc.setConnectTimeout(10000);
            uc.setReadTimeout(10000);
            uc.connect();
            try {
                final int responseCode = uc.getResponseCode();
                if(responseCode != 200) {
                    throw new Exception("Error from Yandex API");
                }
                InputStreamReader inputStream = new InputStreamReader(uc.getInputStream(), encoding);
                final StringBuilder outputBuilder = new StringBuilder();
                BufferedReader reader = new BufferedReader(inputStream);
                String string;
                while (null != (string = reader.readLine())) {
                    outputBuilder.append(string);
                }
                String queryResult = outputBuilder.toString();
                JSONObject jObject = new JSONObject(queryResult);
                String translation =  jObject.getJSONArray("text").getString(0);
                if (translation.equalsIgnoreCase(query)) throw new Exception("No translation");
                String language = null;
                try {
                    language = jObject.getJSONObject("detected").getString("lang");
                } catch (Exception e) {}
                result = new Word(query, translation, language);
            } catch (IOException e) {
                ret = TranslationCode.TRANSLATION_ERROR;
            } catch (Exception e) {
                ret = TranslationCode.NO_TRANSLATION;
            } finally {
                uc.disconnect();
            }
        } catch (IOException e) {
            ret = TranslationCode.TRANSLATION_ERROR;
        } catch (Exception e) {
            ret = TranslationCode.NO_TRANSLATION;
        }
        return ret;
    }

    @Override
    protected void onPostExecute(TranslationCode code) {
        if (!isCancelled()) {
            NewWordFragment refFragment = newWordReference.get();
            if (refFragment != null) {
                refFragment.setTranslate(result, code);
            }
        }
    }

    public static void translate(NewWordFragment refFragment, CharSequence text) {
        stop();
        translator = new YandexTranslator(refFragment);
        translator.execute(text.toString());
    }

    public static void stop() {
        if (translator != null) {
            translator.cancel(true);
            translator = null;
        }
    }
}
